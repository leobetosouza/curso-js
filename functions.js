function somar (x, y) {
	return x + y;
}

function multiplicar (x, y) {
	return x * y;
}

function calcular ( f, x, y ) {
	return f(x, y);
}

var x = calcular( somar, 3, 2 );
var y = calcular( multiplicar, 3, 2 );

function criar_funcao (f, x) {
	return function( a ) {
		return f(x, a);
	}
}

var soma1 = criar_funcao(soma, 1);

var z = soma1(6);


function criar_contador (x) {

	if ( x == null ) x = 0;

	return function() {
		return ++x;
	}
}


var f = criar_contador();
var g = criar_contador();

f()

var h = criar_contador()







