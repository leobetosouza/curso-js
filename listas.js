(function($){
	$(function(){
		var lista = $("#lista");

		var bt = $("#ordenar");

		var callback = function(ev) {

			var items = lista.find("li");

			items.sort(function( a, b ){
				if ( a.textContent > b.textContent ) {
					return 1;
				}
				if ( a.textContent < b.textContent ) {
					return -1;
				}

				return 0
			});

			lista.append(items);

			ev.preventDefault();
		};

		bt.on("click", callback);

		lista.find("a").on("click", function(ev) {

			var target = $("#target");

			$("#target").val( $(this.hash).text() );

			ev.preventDefault();
		});
	});
}(jQuery));