function soma (a, b) {
	return a + b;
}

function mult (a, b) {
	return a * b;
}

function calc ( a, f ) {
	
	var func = function( x ) {
		return f( a, x );
	};

	return func;

}

var func = 1;